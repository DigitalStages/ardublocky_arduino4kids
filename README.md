# ArduBlockly_Arduino4Kids

Data for a course for kids to teach basics of physical computing. ArduBlockly is a block-based coding environment, similar to Scratch and BlocksCAD. The Arduino Kit used for this course is Elegoo "The Most Complete Starter Kit" UNO R3 Project.

![](images/ArduBlocky_Arduino4Kids_FeatureImg.png)

#### Content

* files/arduino_libraries >> All essential libraries for Arduino Software to make ArduBlockly work
* files/code >> Block code files of each tutorial
* files/images >> Images for Tutorial Guide
* files/utilities >> Fritzing parts, additional support files
* images >> Flyers and feature image

----

#### [English Version](files/ArduBlockly_Arduino4Kids_en.md)

[Flyer (English)](images/flyer_en.png)

#### [Deutsche Version](files/ArduBlockly_Arduino4Kids_de.md)

[Flyer (Deutsch)](images/flyer_de.png)
